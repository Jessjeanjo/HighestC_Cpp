//
//  Inheritance_Exam.cpp
//  
//
//  Created by Jessica Joseph on 7/22/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//

#include <iostream>

using namespace std;

class Vehicle{
    int * VIN;
    double * gasMileage;
    
public:
    Vehicle(){ *VIN = 000; *gasMileage = 10.0;}
    Vehicle(int vehicleID){ *VIN = vehicleID; *gasMileage = 10.0;}
    Vehicle(double gasMile){ *VIN = 000, *gasMileage = gasMile;}
    Vehicle(int vehicleID, double gasMile){ *VIN = vehicleID, *gasMileage = gasMile;}
    
    int getVIN() const {return *VIN;}
    double getGasMileage() const {return *gasMileage;}
    
    void changeVIN(int newVIN){ *VIN = newVIN;}
    void changeGasMileage(double newGas){ *gasMileage = newGas;}
    
    double calGasUsed(double milesDriven){return milesDriven/(*gasMileage);}
    void drive();
    void display(){cout<<"You are now in the Vehicle class Display method" << endl;}
    
virtual ~Vehicle(){ cout << "I just got destroyed, Whoopa!!" <<endl;}
};


void Vehicle::drive(){
    int milesDriven = rand() % 100;
    cout << "The miles Driven: " << calGasUsed(milesDriven) << endl;
}

class SUV: public Vehicle{
    bool * fourWDStatus;
    double * fourWDGasMileage;
    
public:
    SUV(): Vehicle(){ *fourWDStatus = true; *fourWDGasMileage = 18;}
    SUV(bool WDState, double WDGas);
    
    bool getFourWDStatus() const { return *fourWDStatus;}
    double getFourWDGasMileage() const { return *fourWDGasMileage;}
    
    void changeFourWDStatus( bool newState){ *fourWDStatus = newState;}
    void changeFourWDGasMileage(double newGasMile);
    void display(){cout<<"You are now in the SUV class Display method" << endl; Vehicle::display();}
    double calGasUsed(double milesDriven);

virtual ~SUV(){ cout << "I just got destroyed, Whoopa!!" <<endl;}

};

SUV::SUV(bool WDState, double WDGas):Vehicle(WDGas){
    *fourWDStatus = WDState;
    
    if ( WDGas <= 0)
        *fourWDGasMileage = 0;
    else
        *fourWDGasMileage = WDGas;

}


void SUV::changeFourWDGasMileage( double newGasMile){
    
    if ( newGasMile <= 0)
        return;
    else
        *fourWDGasMileage = newGasMile;
}

double SUV::calGasUsed(double milesDriven){
    
    if (*fourWDStatus == false)
        return Vehicle::calGasUsed(milesDriven);
    else
        return milesDriven/(*fourWDGasMileage);
}


int main(){
    int arrSize = 3;
    
    cout << "Creating the array"<<endl;
    Vehicle **myVehArr = new Vehicle*[arrSize];
    
    cout <<"Finished creating the array"<<endl;

    SUV mySUV, mySUVTwo(false, 13);
    Vehicle myVeh;
    
    myVehArr[0] = &mySUV;
    myVehArr[1] = &myVeh;
    myVehArr[2] = &mySUVTwo;
    
    for (int i = 0; i < arrSize; i++)
        myVehArr[i]->drive();

    
    delete [] myVehArr;
    
}
