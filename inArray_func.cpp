//
//  Real_Array_P1.cpp
//
//
//  Created by Jessica Joseph on 6/14/15.
//
//

#include <iostream>

using namespace std;

bool isInArray(int testArray[50], int arrSize, int testInt){
//  A function named isInArray that takes an array of ints and its size
//  and an int to search for and returns whether or not that searched-for int is in the passed in array.
    
    for (int indexC = 0; indexC < arrSize; indexC++){
        if (testArray[indexC] == testInt )
            return true;
    }
    
    return false;
}
