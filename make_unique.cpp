//
//  make_unique.cpp
//
//  Created by Jessica Joseph on 06/15/15.
//

void makeUnique(int testArray[arrayMax], int fillArray[arrayMax], int arrSize){
    //  Take an array of ints and its size which may contain duplicate entries
    //  Fills a second vector (which is also passed in as an empty vector) with the ints from the first vector removing all duplicates.
    
    int returnIndex = 0;
    
    for (int indexCounter = 0; indexCounter < arrSize; indexCounter++){
        if (! isInArray(&fillArray[arrayMax], arrSize, testArray[indexCounter])){
            fillArray[returnIndex] = testArray[indexCounter];
            returnIndex = returnIndex + 1;
        }
    }
}
