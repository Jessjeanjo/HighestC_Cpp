//
//  delivery_truck_class.cpp
//
//
//  Created by Jessica Joseph on 6/18/15.
//
//

#include <iostream>
#include <string>
#include <vector>
#include <fstream>

using namespace std;

class DeliveryTruck {
    int truckID;
    string sideAdvertisement = "Default Advertisement";
    vector<double> thePackages;
    vector<int> thePackagesID;
    double loadedTruckWeight = 0;
    
    
public:
    DeliveryTruck(int truckIdentifier){ truckID = truckIdentifier;}
    int getTruckID()const{ return truckID;}
    string getAdvertisement()const{ return sideAdvertisement;}
    void displayAdvertisement();
    void changeAdvertisement(string newAd){sideAdvertisement = newAd;}
    void displayLoadedTruckWeight(){ cout << "\n" <<loadedTruckWeight<< endl;}
    //    void
    void displayCargoWeight();
    void addPackages();
    bool searchPackageID(int);
    void displayTruck();
    
    
};

void DeliveryTruck::displayAdvertisement(){
    cout << sideAdvertisement;
}


ifstream getValidFileStream(){
    string fileName;
    
    cout << "Enter the name of your file, include the extension. i.e .txt or .pdf" << endl;
    getline(cin, fileName);
    
    ifstream readingFile(fileName);
    
    while (!readingFile){
        
        cout << "File NOT found, try again! Remember, include the extension. i.e .txt or .pdf" << endl;
        getline(cin, fileName);
        readingFile.clear();
        
        ifstream readingFile(fileName);
        
    }
    
    return readingFile;
}

void DeliveryTruck::addPackages(){
    double packageWeight; ifstream readingFile; int packageID;
    
    readingFile = getValidFileStream();
    while (readingFile >> packageID ){
        thePackagesID.push_back(packageID);
        
        readingFile >> packageWeight;
        thePackages.push_back(packageWeight);
        loadedTruckWeight += packageWeight;
    }
    
}

void DeliveryTruck::displayCargoWeight(){
    for ( int packageIndex = 1; packageIndex < (thePackages.size()+1); packageIndex++){
        cout << "Package Number " << packageIndex << " has a weight of " << thePackages[packageIndex-1] << endl;
    };
}


bool DeliveryTruck::searchPackageID(int searchPackage){
    for (int index = 0; index < thePackagesID.size(); index++){
        if (thePackagesID[index] == searchPackage)
            return true;
    }
    
    return false;
}

void DeliveryTruck::displayTruck(){
    cout << "\n\nDISPLAYING TRUCK" <<endl;
    cout << "\nDelivery Truck ID: " << getTruckID() << endl;
    cout << "This is Truck Number " << getTruckID() << "'s side advertisement" << getAdvertisement() << endl;
    
}

int main (){
    
    int menuOptionChoice = 0, packageSearchID;
    string advertisementStr; ifstream packageFile;
    int idTruck = 8766; bool searchResult;
    
    DeliveryTruck defaultTruck(idTruck);
    
    do {
        cout << "\n\nWelcome to Jessica's Delivery Truck System\nLet's get started!\n\n" << endl;
        cout << "Type in the number that corresponds with your choice!\n\n1. Check out the Truck\n" \
        "2. Change Advertisement\n3. Add new packages\n4. Display Weight of Individual Cargo\n5. Display loaded truck weight\n"\
        "6.Search for Package on Truck\n7. Quit" << endl;
        
        cin >> menuOptionChoice;
        if (menuOptionChoice != 7){
            if (menuOptionChoice == 1)
                defaultTruck.displayTruck();
            else if (menuOptionChoice == 2){
                cout <<"What is your new advertisement?" << endl;
                cin >> advertisementStr;
                defaultTruck.changeAdvertisement(advertisementStr);
                cout << "Your advertisement has been changed!" << endl;
            }
            else if (menuOptionChoice == 3)
                defaultTruck.addPackages();
            else if (menuOptionChoice == 4)
                defaultTruck.displayCargoWeight();
            else if (menuOptionChoice == 5)
                defaultTruck.displayLoadedTruckWeight();
            else {
                cout << " What is your package search ID? " << endl;
                cin >> packageSearchID;
                searchResult = defaultTruck.searchPackageID(packageSearchID);
                if (searchResult)
                    cout << "Your package is here!" << endl;
                else
                    cout << "Sorry, not here boss" << endl;
            }
            
            
        } else
            cout << "\nThank you for visiting, GoodBye!" << endl;
        
    } while (menuOptionChoice != 7);
    
    
}

