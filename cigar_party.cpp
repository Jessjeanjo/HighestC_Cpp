//
//  cigar_party.cpp
//  Cpp_HomeP
//
//  Created by Jessica Joseph on 5/27/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//
//  When squirrels get together for a party, they like to have cigars.
//  A squirrel party is successful when the number of cigars is between 40 and 60, inclusive.
//  Unless it is the weekend, in which case there is no upper bound on the number of cigars.
//  Returns True if the party with the given values is successful, or False otherwise.
//
//

#include <iostream>

using namespace std;



bool cigar_party(int cigars, bool isWeekend){
    if (isWeekend == true){
        if (cigars >= 40){
            return true;
        } else {
            return false;
        }
    } else{
        if ((40 <= cigars) && (cigars <= 60)){
            return true;
        } else{
            return false;
        }
    }
}

int main(){
    cout << "How many cigars would you like?" << endl;
    int numOfCigars;
    cin >> numOfCigars;
    
    cout << "Is it a weekend? Type 'true' or 'false'" << endl;
    bool isWeekend;
    cin >> isWeekend;
    
    int returnValue;
    returnValue = cigar_party(numOfCigars, isWeekend);
    
    if (returnValue == 1){
        cout << "Enjoy your cigars, great party!!!" << endl;
    } else {
        cout << "You do not have enough cigars, not a good party!" << endl;
    }
}
