//
//  answer_checker.cpp
//  Cpp_HomeP
//
//  Created by Jessica Joseph on 6/02/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//
//  Determines if a given addition or subtraction problem was
//  solved correctly.
//

#include <iostream>
#include <string>

using namespace std;

bool correctAnswerBool( double resulNumb, double correctNumb){
    if (resulNumb == correctNumb)
        return true;
    else
        return false;
}


void checkAnswer(double numbOne, double numbTwo, double resultNumb, char strOperator){
    double correctNumb;
    
    if (strOperator == '+')
        correctNumb = numbOne + numbTwo;
     else if (strOperator == '-')
        correctNumb = numbOne - numbTwo;
    
    if (correctAnswerBool(resultNumb, correctNumb))
        cout << "Hey you were correct, congratualtions!!";
    else
        cout << "Sorry you were wrong bruh";

    
}

int main (){
    double numbOne, numbTwo, resultNumb;
    char operatorStr;
    
    cout << "Enter the first number:" << endl;
    cin >> numbOne;
    
    cout << "Enter the second number:" << endl;
    cin >> numbTwo;
    
    cout << "Enter your operation. + or -" << endl;
    cin >> operatorStr;
    
    cout << "Enter your answer" << endl;
    cin >> resultNumb;
    
    checkAnswer(numbOne, numbTwo, resultNumb, operatorStr);
}


