//
//  stringReverse.cpp
//
//
//  Created by Jessica Joseph on 6/23/15.
//
//

#include <iostream>
#include <cstdio>

using namespace std;

void switchVal( char &valOne, char &valTwo){
    //Takes the reference to two char and switches the values stored
    
    char tempSave;
    tempSave = valOne;
    valOne = valTwo;
    valTwo = tempSave;
}

void charFunc(char * cString, int size){
    //Takes in a pointer to an array and its size and switches its values
    
    char *ptrFront, *ptrRear;
    
    ptrFront = &cString[0]; ptrRear = &cString[size-1];
    
    for (int i = 0; i < (size/2); i++){
        switchVal(*ptrFront, *ptrRear);
        ptrFront++;
        ptrRear--;
    }
}

int main(){
    char string[256]; int size;
    
        cin.getline( string, 256, '\n');
    cout << "\nSize of what you wrote please\n" << endl;
    cin >> size;
    
    charFunc(string, size);
    
        cout << string << endl;
    return 0;
}
