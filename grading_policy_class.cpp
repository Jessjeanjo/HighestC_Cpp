//
//  Grading_Polices_01.cpp
//  
//
//  Created by Jessica Joseph on 7/10/15.
//
//

#include <iostream>

using namespace std;

class Grading {
    double quizO;
    double quizT;
    double midterm;
    double finalExam;
    
public:
    Grading();
    Grading(double fQuiz, double tQuiz, double midExam, double fExam);
    
    void changeQuizO( double newVal){ quizO = newVal;}
    void changeQuizT( double newVal){ quizT = newVal;}
    void changeMidterm( double newVal){ midterm = newVal;}
    void changeFinal( double newVal){ finalExam = newVal;}

    char calLetter();
};

Grading::Grading(){
    quizO = 10;
    quizT = 10;
    midterm = 100;
    finalExam = 100;
}

Grading::Grading( double fQuiz, double tQuiz, double midExam, double fExam){
    quizO = fQuiz;
    quizT = tQuiz;
    midterm = midExam;
    finalExam = fExam;
}

char Grading::calLetter(){
    double quizTot, midTot, finalTot, classTot;
    quizTot = (quizO + quizT)*0.25;
    midTot = midterm*0.25;
    finalTot = finalExam*0.50;
    
    classTot = quizTot + midTot + finalTot;
    
    if (classTot >= 90)
        return 'A';
    else if (classTot >= 80)
        return 'B';
    else if (classTot >= 70)
        return 'C';
    else if (classTot >= 60)
        return 'D';
    else
        return 'F';
}

int main(){
    cout << "You're good boss"<<endl;
    
}
