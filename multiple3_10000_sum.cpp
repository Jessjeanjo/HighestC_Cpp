//
//  multiple3_10000_sum.cpp
//  Cpp_HomeP
//
//  Created by Jessica Joseph on 5/27/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//
//  Sums the values that are multiples
//  of 3 from 0 to 10,00 inclusively
//

#include <iostream>

using namespace std;





int main(){
    int total = 0;
    int upperBound = 10000;
    for (int i = 0; i <= upperBound; i++){
        if (i % 3 == 0){
            total += i;
        }
        
    }
    cout << "Sum of the numbers that are multiples of 3 from [0," << upperBound << "]: " << total;
}


