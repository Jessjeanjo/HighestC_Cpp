//
//  Stack.cpp
//  
//
//  Created by Jessica Joseph on 7/6/15.
//
//

#include <iostream>

using namespace std;


class stack{
    int capacity;
    int size;
    int *intPtr;
    void resizeFunction(int newSize);
    void storeAtEnd( int storeItem);
    
public:
    stack(){ capacity = 100; size = 15; intPtr = new int[50];}
    stack(int * theArray, int cap, int size2){ capacity = cap; size = size2; intPtr = new int[cap];}
    
    void push(int newItem);
    int pop();
    int top();
    void clear();
    bool isEmpty();
    int capacityFunc(){ return capacity;}

    //Operator Overloading
    int operator[](int val);
    stack & operator=(const stack& rhs);
    
    //Destructor
    ~stack(){ delete [] intPtr;}
    
    //Copy Constructor
    stack(const stack& copyStack);
};

//Overloading Operators
int stack::operator[](int arrIndex){
    for (int i = 0; i < arrIndex; i++)
        intPtr++;
    
    return *intPtr;
}

stack& stack::operator=(const stack&rhs){
    
    if ( this == &rhs)
        return *this;
    
    delete [] intPtr;
    
    intPtr = new int[rhs.capacity];
    *intPtr = *rhs.intPtr;
    
    return *this;
}

//Copy Constructor
stack::stack(const stack& copyStack){
    capacity = copyStack.capacity;
    size = copyStack.size;
    intPtr = copyStack.intPtr;
}


//Stack Functions
void  stack::resizeFunction( int newSize){
    
    int *heapPointy = new int[newSize];
    
    if (newSize < size){
        for ( int i = 0; i < newSize; i++){
            heapPointy = intPtr;
            heapPointy++;
        }
        
    } else {
        for ( int i = 0; i < size; i++){
            heapPointy = intPtr;
            heapPointy++;
        }
        
        
    }
    delete [] intPtr;
    intPtr = heapPointy;
    size = newSize;
    
}

void stack::storeAtEnd(int storeItem){
    int newSize = size+1;
    resizeFunction( newSize);
    
    intPtr[size] = storeItem;
    
}

void stack::push(int newItem){
    storeAtEnd(newItem);
}

int stack::pop(){
    for (int i = 0; i < size; i++)
        intPtr++;
    
    int returnVal = *intPtr, newSize = size-1;
    
    resizeFunction(newSize);
    return returnVal;
}

int stack::top(){
    for (int i = 0; i < size; i++)
        intPtr++;
    
    int returnVal = *intPtr;
    return returnVal;
}

void stack::clear(){
    delete [] intPtr;
    intPtr = new int[capacityFunc()];
}

bool stack::isEmpty(){
    if (intPtr == NULL)
        return true;
    else
        return false;
}




