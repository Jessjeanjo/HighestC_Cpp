//
//  Real_Array.cpp
//
//
//  Created by Jessica Joseph on 6/14/15.
//
//

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

const int arrayMax = 50;

bool isInArray(int testArray[arrayMax], int arrSize, int testInt){
    //  A function named isInArray that takes an array of ints and its size
    //  and an int to search for and returns whether or not that searched-for int is in the passed in array.
    
    for (int indexC = 0; indexC < arrSize; indexC++){
        if (testArray[indexC] == testInt )
            return true;
    }
    
    return false;
}

void makeUnique(int testArray[arrayMax], int fillArray[arrayMax], int arrSize){
    //  Take an array of ints and its size which may contain duplicate entries
    //  Fills a second vector (which is also passed in as an empty vector) with the ints from the first vector removing all duplicates.
    
    int returnIndex = 0;
    
    for (int indexCounter = 0; indexCounter < arrSize; indexCounter++){
        if (! isInArray(&fillArray[arrayMax], arrSize, testArray[indexCounter])){
            fillArray[returnIndex] = testArray[indexCounter];
            returnIndex = returnIndex + 1;
        }
    }
}


ifstream getValidFileStream(){
    //  Retrieves a valid file from the caller. Does not continue if the file given does not open properly.
    //  Returns a valid reading file handler
    
    string fileName;
    
    cout << "Enter the name of your file, include the extension. i.e .txt or .pdf" << endl;
    getline(cin, fileName);
    
    ifstream readingFile(fileName);
    
    while (!readingFile){
        
        cout << "File NOT found, try again! Remember, include the extension. i.e .txt or .pdf" << endl;
        getline(cin, fileName);
        readingFile.clear();
        
        ifstream readingFile(fileName);
        
    }
    
    return readingFile;
}



int fillArrFromFile(int fillArr[arrayMax]){
    // Fills a array of ints with the data from the caller's chosen input file
    // Returns the array size
    
    int newElement, indexCounter = 0, arrySize = 0;
    ifstream readingFile;
    
    readingFile = getValidFileStream();
    
    while (readingFile >> newElement){
        fillArr[indexCounter] = newElement;
        arrySize += 1; indexCounter ++;
    }
    return  arrySize;
}

void writeVecFile(int writeArray[arrayMax], int arrSize){
    // Writes the values in an array to a file
    // File name is chosen by the caller, nothing is returned
    
    string fileName;
    
    cout << "Enter the name you want your file to be called, include the extension. i.e .txt or .pdf" << endl;
    getline(cin, fileName);
    
    ofstream writingFile(fileName);
    for (int indexCounter = 0; indexCounter < arrSize; indexCounter++){
        writingFile << writeArray[indexCounter] << endl;
    }
    
    writingFile.close();
    cout << "All done, boss" << endl;
}

int main(){
    int mainArr[arrayMax], uniqueArray[arrayMax], arraySize;
    arraySize = fillArrFromFile(& mainArr[arrayMax]);
    makeUnique(&mainArr[arrayMax], &uniqueArray[arrayMax], arraySize);
    writeVecFile(uniqueArray, arraySize);
    
}
