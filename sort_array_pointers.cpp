//
//  Pointers.cpp
//  Created by Jessica Joseph on 6/10/15.
//

#include <iostream>

using namespace std;

const int ARR_MAX = 50;

void switchValue( int &valOne, int &valTwo){
    //Takes the reference to two ints and switches the values stored
    
    int tempSave;
    tempSave = valOne;
    valOne = valTwo;
    valTwo = tempSave;
}


void sortArray ( int *testArray, int arraySize){
    // Takes in a size (int) and a pointer to an integer dynamic array and sorts the dynamic array
    // Sorting is done by creating a new dynamic array, copying over the data and returning the pointer to the new array
    
    
    int *testArrayPointer, *minVal;
    testArrayPointer = new int[arraySize];
    
    testArrayPointer = testArray;
    minVal = &testArrayPointer[0];

    int tester = 0;
    
    while (tester < arraySize){

        for (int indexVal = 0; indexVal < arraySize; indexVal++){
            if (*minVal > testArrayPointer[indexVal]){
                switchValue(testArrayPointer[indexVal], *minVal);
            }
        }
        
        minVal++; tester++;
    }
    
}



int main(){
    int arraySize = 9;
    
    int testArray[ARR_MAX] = {3,41, 94, 7, 5,80,21,43,100};
    
    sortArray(testArray, arraySize);
    
    for (int index = 0; index < arraySize; index++){
        cout << testArray[index] << endl;
    }
    
    cout << "\nAll done, boss!" << endl;
    
}
