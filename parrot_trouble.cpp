//
//  parrot_trouble.cpp
//  Cpp_HomeP
//
//  Created by Jessica Joseph on 5/28/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//
//  There is a loud talking parrot. The "hour" parameter is the current hour time in the range 0..23.
//  We are in trouble if the parrot is talking and the hour is before 7 or after 20. Return True if we are in trouble. //


#include <iostream>
#include <string>

using namespace std;



bool parrot_trouble( bool talking, int hour){
    if (talking == "true" && (hour < 7 || hour > 20)){
        return true;
    } else {
        return false;
    }
}

int main (){
    string talkingVar, returnVar; int hourVar;
    
    cout << "Is the parrot talking? If the parrot is talking type 'true', otherwise type 'false'" << endl;
    getline(cin, talkingVar);
    
    cout << "What hour of the day is it? (0 to 23 hour scale)" << endl;
    getline(cin, hourVar); 
    returnVar = parrot_trouble(talkingVar, hourVar);
    
    if (returnVar){
        cout << "The parrot is a bad parrot. This is not the time to talk!!" << endl;
    } else {
        cout << "I love the way the parrot sings. A beautiful time to sing" << endl;
    }
}
