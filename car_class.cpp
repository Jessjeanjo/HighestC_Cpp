//
//  car_class.cpp
//
//
//  Created by Jessica Joseph on 6/18/15.
//
//

#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

const int STAND_EFF_RATE = 25;

class Car {
    string model;
    string make;
    string licensePlateNumb;
    double gasTank = 15.0; //15 is the amount of gallons of a full tank
    int odometer;
    int efficiencyRate;
    
public:
    Car(string modelP = "unspecified", string makeP = "unspecified", int efficiencyR = STAND_EFF_RATE);
    
    void changeLicensePlate( string newLicense){ licensePlateNumb = newLicense;}
    
    string getModel()const {return model;}
    string getMake()const {return make;}
    string getLicensePlateNumb()const {return licensePlateNumb;}
    double getGasTank()const {return gasTank;}
    int getEfficiencyRate()const {return efficiencyRate;}
    
    void fillUpGasTank(double gasAmnt){ gasTank += gasAmnt;}
    double drive();
    
    void displayTotalDis(){ cout << odometer <<endl;}
    void displayModel(){ cout << model <<endl;}
    void displayMake(){ cout << make <<endl;}
    void displayLicense(){ cout << licensePlateNumb <<endl;}
    void displayEfficiencyRate(){ cout << efficiencyRate <<endl;}
    void displayGasTank(){ cout << gasTank <<endl;}
    
    
};

Car::Car(string modelP, string makeP, int effRate ){
    model = modelP;
    make = makeP;
    efficiencyRate = effRate;
}


double Car::drive(){
    
    int milesDriven;
    srand(time(NULL));
    milesDriven = rand()% 25 +1;
    odometer += milesDriven;
    
    if ((gasTank -= milesDriven/STAND_EFF_RATE) <= 0 ){
        cout << "You cannot drive another " << milesDriven << " miles you will be stranded!\n" << endl;
        gasTank += (milesDriven/STAND_EFF_RATE);
    } else {
        cout << "You just drove " << milesDriven << " miles" << endl;
        gasTank -= (milesDriven/STAND_EFF_RATE);}
    
    return milesDriven;
}


int main(){
    string make, model;
    
    model = "Hybrid";
    make = "AWDLmt";
    
    Car Jess(model, make);
    
    Jess.drive();
    Jess.displayGasTank();
}
