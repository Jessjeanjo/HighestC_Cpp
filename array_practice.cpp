//
//  array_practice.cpp
//
//  Created by Jessica Joseph on 6/14/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//

#include "eval.h"
#include <iostream>

using namespace std;

int main(){
    
    int score[5];
    int n = 6;
    
    for (int i = 0; i < 5; i++){
        score[i] = n;
        n++;
    }
    
    for (int i = 0; i < 5; i++){
        cout << "The value at position " << i+1 <<" is:\n" << score[i] << endl;
    }
}
