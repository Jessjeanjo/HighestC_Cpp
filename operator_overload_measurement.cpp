//
//  Operator Overloading.cpp
//  
//
//  Created by Jessica Joseph on 7/6/15.
//
//

#include <iostream>
#include <fstream>
#include <string>

using namespace std;


class Measurement{
    int feet;
    int inches;
    void inchCheckAdjust(int inchVal);
    
public:
    Measurement(){ feet = 0; inches = 0;}
    Measurement(int inchVal){ inchCheckAdjust(inchVal);}
    int getFeet()const{return feet;}
    int getInches()const{return inches;}
    
    //Operator Overloading
    Measurement operator+(const Measurement& rhs);
    Measurement operator-(const Measurement& rhs);
    
    //Comparison Operator
    bool operator<(const Measurement& rhs);
    bool operator==(const Measurement& rhs){return !(operator<(rhs) || !(operator<(rhs)));}
    bool operator!=(const Measurement& rhs){return operator<(rhs) || !(operator<(rhs));}
    
    //Incremnetation
    void operator++(){ inches += 1;}
    void operator++(int){inches += 1;}
    
    //IO Operators
    friend ostream& operator<<(ostream& outs, const Measurement& rhs);
    
};

void Measurement::inchCheckAdjust(int inchVal){
    int addOn = 0;
    
    if (inchVal > 11){
        while (inchVal > 11){
            inchVal -= 12;
            addOn ++;
        }
        feet = addOn;
    }else if (inchVal < 0){
        while (inchVal < 0){
            inchVal += 12;
            addOn ++;
        }
        feet = addOn;
    }
    
    inches = inchVal;
}

bool Measurement::operator<(const Measurement& rhs){
    int lhsTot, rhsTot;
    
    lhsTot = (this->feet)*12 + this->inches;
    rhsTot = (rhs.feet)*12 + rhs.inches;
    
    if (lhsTot < rhsTot)
        return true;
    else
        return false;
}


ostream& operator<<(ostream& outs, Measurement& rhs){
    return outs << rhs.getFeet()<<"\'"<< rhs.getInches()<<"\""<< endl;
}


int main(){
    
    Measurement valO(76), valT(76), valTh(76);
    bool result;
    
    cout << valO;
    cout << valT;
    
    cout <<"Post increment-> ";
    valO++;
    cout << valO;
    
    cout <<"\nPre increment-> ";
    ++valO;
    cout <<valO;
    
    cout << "\nTesting to see if the Measurements are not equal" <<endl;
    result =  (valTh != valT);
    if ( result == 1 )
        cout << "That is correct! They are not equal."<< endl;
    else
        cout << "You are wrong, They are equal!"<< endl;
    
    cout << "\nTesting to see if the Measurements are equal" <<endl;
    result =  (valTh == valT);
    if ( result == 1 )
        cout << "That is correct! They are equal."<< endl;
    else
        cout << "You are wrong, They are not equal"<< endl;
    
}
