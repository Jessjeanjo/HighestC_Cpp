//
//  monkey_trouble.cpp
//  Cpp_HomeP
//
//  Created by Jessica Joseph on 5/27/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//
//  We have two monkeys, a and b, and the parameters a_smile and b_smile indicate if each is smiling.
//  We are in trouble if they are both smiling or if neither of them is smiling. Return True if we are in trouble.
//

#include <iostream>
#include <string>

using namespace std;

bool monkey_trouble(string a_smile, string b_smile){
    if ((a_smile == "true" && b_smile == "true") || (a_smile == "false" && b_smile == "false")){
        return true;
    } else {
        return false;
    }
}


int main () {
    string monkey_a, monkey_b;
    bool monkeyResult;
    
    cout << "Is the first monkey smiling? Enter 'true' or 'false'" << endl;
    getline(cin, monkey_a);
    
    cout << "Is the second monkey smiling? Enter 'true' or 'false'" << endl;
    getline(cin, monkey_b); 
    
    monkeyResult = monkey_trouble(monkey_a, monkey_b);
    
    if (monkeyResult){
        cout << "Freak out, everyone! Everything is NOT OKAY!!!" << endl;
    } else {
        cout << "Everything is all good!! Enjoy life, the monkeys are happy." << endl;
    }
    
   
    
}
