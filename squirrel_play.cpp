//
//  squrrielPlay.cpp
//  Cpp_HomeP
//
//  Created by Jessica Joseph on 5/26/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//

#include <iostream>

using namespace std;


bool squirrelPlay(int temp, bool isSummer){
    /*The squirrels in Palo Alto spend most of the day playing. In particular, they play if the temperature is
     between 60 and 90 (inclusive). Unless it is summer, then the upper limit is 100 instead of 90.
     Given an int temperature and a boolean is_summer, return True if the squirrels play and False otherwise.*/
    if (isSummer == true){
        if (temp >= 60 && temp<=100){
            return "The squirrels will play today!";
        } else{
            return "The squirrels will NOTT play today!";
        }
    } else {
        if (temp >= 60 && temp <= 90){
            return "The squirrels will play today!";
        } else {
            return "The squirrels will NOTT play today!";
        }
    }
}


int main(){
    cout << "Enter the temperature?\n";
    int x;
    cin >> x;
    
    cout << "Is it summer? If it is summer enter 'true', if it not summer enter 'false' without the quotes('')\n ";
    int y;
    cin >> y;
    cout << squirrelPlay(x, y);
    return 0;
}
