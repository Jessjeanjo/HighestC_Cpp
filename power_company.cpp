//
//  power_company.cpp
//
//  Created by Jessica Joseph on 6/3/15.
//  Copyright (c) 2015 Jessica Joseph. All rights reserved.
//
//
//  The power company reads meters each month to determine how many kilowatt-hours you’ve used.  Every meter has an ID. 
//  Sometimes the meter reader misses a meter.
//  The company saves the readings in a file where the first column is the meter ID and the second is the reading.  
//  Ask the user for two file names (from two months) and calculate the utilization for each meter.   
//  The utilization will be the absolute value of the difference between the two meter readings.   Print the number of kilowatt-hours used for each meter and print a message if there are any meters in the second month that are not in the first month or vice versa.


#include <iostream>
#include <string>
#include <fstream>
#include <cmath>

using namespace std;

const int ERROR = 1;

ifstream openFile(string message, string errorMess = "Your file was not found, please try again!"){
    string fileName;
    cout << message << endl;
    getline(cin, fileName);
    
    ifstream inputFile;
    inputFile.open(fileName);
    
    while (!inputFile){
        cout << errorMess << endl;
        getline(cin, fileName);
        
        inputFile.clear();
        
        ifstream inputFile;
        inputFile.open(fileName);
    }
    
    return inputFile;
}

double utilCal(double sumOne, double sumTwo){
    return abs(sumTwo - sumOne);
}

void printSum(double readingOne, double readingTwo){
    cout << "Total reading for month One\t" << readingOne << "\tkilowatt-hours" << endl;
    cout << "Total reading for month Two\t" << readingTwo << "\tkilowatt-hours" << endl;
}

int handleFile(ifstream meterFileO, ifstream meterFileT){
    
    string meterTwoID = 0, meterOneID = 0, meterOneIDCount, meterTwoIDCount;
    double meterOneSum = 0, meterTwoSum = 0, meterOneCount = 0, meterTwoCount = 0;
    
    while (meterFileO >> meterOneID >> meterOneSum){
        meterOneCount += meterOneSum;
        meterOneIDCount += meterOneID;
        
    }
    
    while (meterFileT >> meterTwoID >> meterTwoSum){
        meterTwoCount += meterTwoSum;
        meterTwoIDCount += meterTwoID;
    }
    
    printSum(meterOneSum, meterTwoSum);
    
    if (meterTwoIDCount != meterOneIDCount)
        return 1;
    else
        return 0;
}


int main(){
    
    if (handleFile(openFile("Enter the file name of your First meter: "), openFile("Enter the file name of your Second meter: ")) == ERROR)
        cout << "There is a meter that was only found in one bill" << endl;
    else
        cout << "Everything is all good, boss!" << endl;

    return 0;
}
