//
//  Operator Overloading_Angle.cpp
//  
//
//  Created by Jessica Joseph on 7/6/15.
//
//

#include <iostream>
#include <vector>
#include <string>
#include <fstream>

using namespace std;

const int DEGREE = 0, MINUTE = 1, SECOND = 2;

class Angle{
    int degVal;
    int minVal;
    int secVal;
    void GetDataKB(int postion, int newVal);
    
public:
    Angle();
    Angle(int degree, int minute, int second);
    Angle( int position, int newVal);
    int getDegree()const{return degVal;}
    int getMinute()const{return minVal;}
    int getSecond()const{return secVal;}
    void changeDegree(int newVal){ GetDataKB(0, newVal);}
    void changeMinute(int newVal){GetDataKB(1, newVal);}
    void changeSecond(int newVal){GetDataKB(2, newVal);}
    
    //Overloaded Operators
    Angle operator+(const Angle& rhs);
    Angle operator-(const Angle & rhs);
    
    //Comparison Operators
    bool operator<(const Angle & rhs);
    bool operator>(const Angle & rhs){return !operator<(rhs);}
    bool operator==(const Angle & rhs){return !(operator<(rhs) || !(operator<(rhs)));}
    
    //IO Operators
    friend ostream& operator<<(ostream& outs, const Angle& rhs);

};

Angle::Angle(){
    degVal = 0; minVal = 0; secVal = 0;
}


Angle::Angle(int degree, int minute, int second){
    degVal = degree;
    minVal = minute;
    secVal = second;
    
}
Angle::Angle(int position, int newVal){
    GetDataKB(position, newVal);
}

void Angle::GetDataKB(int position, int newVal){
    
    if (position == DEGREE)
        degVal = newVal;
    else if (position == MINUTE)
        minVal = newVal;
    else
        secVal = newVal;
    
    
}

Angle Angle:: operator+(const Angle & rhs){
    
    Angle returnObj;
    
    int addOn = 0, secVal = this->secVal + rhs.secVal;
    
    while (secVal > 60){
        secVal -= 60;
        addOn++;
    }
    returnObj.secVal = secVal;
    int minVal = this->minVal + rhs.minVal+ addOn;
    
    addOn = 0;
    while (minVal > 60){
        minVal -= 60;
        addOn++;
    }
    
    returnObj.degVal = this->degVal + rhs.degVal+ addOn ;
    
    
    return returnObj;
}

Angle Angle:: operator-(const Angle & rhs){
    
    Angle returnObj;
    
    int addOn = 0, secVal = this->secVal - rhs.secVal;
    
    while (secVal > 60){
        secVal -= 60;
        addOn++;
    }
    returnObj.secVal = secVal;
    int minVal = this->minVal - rhs.minVal+ addOn;
    
    addOn = 0;
    while (minVal > 60){
        minVal -= 60;
        addOn++;
    }
    
    returnObj.degVal = this->degVal - rhs.degVal+ addOn ;
    
    
    return returnObj;
}


bool Angle::operator<(const Angle & rhs){
    int lhsTotSec, rhsTotSec;
    
    lhsTotSec = (this->degVal)*60 + (this->minVal)*60 + this->secVal;
    rhsTotSec = (rhs.degVal)*60 + (rhs.minVal)*60 + rhs.secVal;
    
    if (lhsTotSec < rhsTotSec)
        return true;
    else
        return false;
}


void operator>>(istream &ins, Angle & rhs){
    int newDeg, newMin, newSec;
    ins >> newDeg >> newMin >> newSec;
    
    rhs.changeDegree(newDeg);
    rhs.changeMinute(newMin);
    rhs.changeSecond(newSec);
    
}

ostream & operator<<(ostream& outs, Angle& rhs){
    return outs << rhs.getDegree()<<":"<< rhs.getMinute()<<":"<< rhs.getSecond();
}


int main(){
    Angle angOne, angTwo, angResult;
    
    cout <<"Give me your FIRST Angle"<<endl;
    cin >> angOne;
    cout <<"\nGive me your SECOND Angle"<<endl;
    cin >> angTwo;
    
    cout << "\nThe sum of your two angles" << endl;
    angResult = angOne + angTwo;
    cout << angResult;
    
    cout << "\nThe difference of your two angles" <<endl;
    angResult = angOne - angTwo;
    cout <<angResult;
    
}
